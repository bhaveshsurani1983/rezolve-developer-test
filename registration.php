<?php
/**
 * Bytes Technolab
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * php version 7.0
 *
 * @category Bytes
 * @package  Bytes_RecCalculator
 * @author   Magento Team <info@bytestechnolab.com>
 * @license  OSL 3.0
 * @link     http://www.bytestechnolab.com
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Bytes_RecCalculator',
    __DIR__
);
