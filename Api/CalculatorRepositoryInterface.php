<?php
/**
 * Bytes Technolab
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * php version 7.0
 *
 * @category Bytes
 * @package  Bytes_RecCalculater
 * @author   Magento Team <info@bytestechnolab.com>
 * @license  OSL 3.0
 * @link     http://www.bytestechnolab.com
 */

namespace Bytes\RecCalculator\Api;

use Magento\Framework\Api\SearchCriteriaInterface;
use Bytes\RecCalculator\Api\Data\RequestInterface;
use Bytes\RecCalculator\Api\Data\ResponceInterface;

/**
 * Interface CalculatorRepositoryInterface
 *
 */

interface CalculatorRepositoryInterface
{
    const ADD       = 'add';
    const SUBTRACT  = 'subtract';
    const MOLTIPLY  = 'multiply';
    const DIVIDE    = 'divide';
    const POWER     = 'power';

    /**
     * Calculat requested data
     *
     * @param RequestInterface $request AnswerObject
     *
     * @return ResponceInterface
     */
    public function calculat(RequestInterface $request);
}
