<?php
/**
 * Bytes Technolab
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * php version 7.0
 *
 * @category Bytes
 * @package  Bytes_RecCalculater
 * @author   Magento Team <info@bytestechnolab.com>
 * @license  OSL 3.0
 * @link     http://www.bytestechnolab.com
 */

namespace Bytes\RecCalculator\Api\Data;

/**
 * Interface ResponceInterface
 *
 */
interface ResponceInterface
{
    const STATUS = 'status';
    const RESULT = 'result';

    /**
     * Set Status
     *
     * @param string $status Status
     *
     * @return DataInterface
     */
    public function setStatus($status);

    /**
     * Get Status
     *
     * @return string
     */
    public function getStatus();

    /**
     * Set Result
     *
     * @param float | int $result Result
     *
     * @return DataInterface
     */
    public function setResult($result);

    /**
     * Get Result
     *
     * @return float | int
     */
    public function getResult();
}
