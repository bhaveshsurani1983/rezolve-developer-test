<?php
/**
 * Bytes Technolab
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * php version 7.0
 *
 * @category Bytes
 * @package  Bytes_RecCalculater
 * @author   Magento Team <info@bytestechnolab.com>
 * @license  OSL 3.0
 * @link     http://www.bytestechnolab.com
 */

namespace Bytes\RecCalculator\Api\Data;

/**
 * Interface RequestInterface
 *
 */
interface RequestInterface
{
    const LEFT         = 'left';
    const RIGHT        = 'right';
    const OPERATOR     = 'operator';
    const PRECISION    = 'precision';
    
    /**
     * Set Left value
     *
     * @param float | int $leftValue Left value
     *
     * @return DataInterface
     */
    public function setLeft($leftValue);

    /**
     * Get Left value
     *
     * @return float | int
     */
    public function getLeft();

    /**
     * Set Right value
     *
     * @param float | int $rightValue Right value
     *
     * @return DataInterface
     */
    public function setRight($rightValue);

    /**
     * Get Right value
     *
     * @return float | int
     */
    public function getRight();

    /**
     * Set Calculator Operator
     *
     * @param string $operator calulator operator
     *
     * @return DataInterface
     */
    public function setOperator($operator);

    /**
     * Get Calculator Operator
     *
     * @return string
     */
    public function getOperator();

    /**
     * Set Precision
     *
     * @param int $precision precision
     *
     * @return DataInterface
     */
    public function setPrecision($precision);

    /**
     * Get Precision
     *
     * @return int
     */
    public function getPrecision();
}
