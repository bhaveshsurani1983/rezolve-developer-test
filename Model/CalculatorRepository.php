<?php
/**
 * Bytes Technolab
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * php version 7.0
 *
 * @category Bytes
 * @package  Bytes_RecCalculater
 * @author   Magento Team <info@bytestechnolab.com>
 * @license  OSL 3.0
 * @link     http://www.bytestechnolab.com
 */

namespace Bytes\RecCalculator\Model;

use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Exception\ValidatorException;
use Bytes\RecCalculator\Api\Data\RequestInterface;
use Bytes\RecCalculator\Api\Data\ResponceInterface;
use Bytes\RecCalculator\Api\CalculatorRepositoryInterface;

/**
 * Calculator repository model
 *
 */
class CalculatorRepository implements CalculatorRepositoryInterface
{
    /**
     * Responce Object
     *
     * @var ResponceInterface $emailSender
     */
    protected $responce;

    /**
     * Default Constructor
     *
     * @param ResponceInterface $responce
     */
    public function __construct(
        ResponceInterface $responce
    ) {
        $this->responce = $responce;
    }

    /**
     * Calculat requested data
     *
     * @param RequestInterface $request requestedData
     *
     * @return ResponceInterface
     *
     * @throws Exception
     */
    public function calculat(RequestInterface $request)
    {
        $result = 0;

        if ($request->getLeft() == null
            || (!is_int($request->getLeft())
            && !is_float($request->getLeft()))) {
            throw new \Exception(__("Error : Invalid left value, Please input integer or float value"));
        } elseif ($request->getLeft() == null
            || (!is_int($request->getRight())
            && !is_float($request->getRight()))) {
            throw new \Exception(__("Error : Invalid right value, Please input integer or float value"));
        }

        if ($request->getOperator() == CalculatorRepositoryInterface::ADD) {
            $result = $this->calculateAddition($request->getLeft(), $request->getRight());
        } elseif ($request->getOperator() == CalculatorRepositoryInterface::SUBTRACT) {
            $result = $this->calculateSubtraction($request->getLeft(), $request->getRight());
        } elseif ($request->getOperator() == CalculatorRepositoryInterface::MOLTIPLY) {
            $result = $this->calculateMultiplication($request->getLeft(), $request->getRight());
        } elseif ($request->getOperator() == CalculatorRepositoryInterface::DIVIDE) {
            try {
                $result = $this->calculateDivide($request->getLeft(), $request->getRight());
            } catch (\Exception $e) {
                throw new \Exception($e->getMessage());
            }
        } elseif ($request->getOperator() == CalculatorRepositoryInterface::POWER) {
            $result = $this->calculatePower($request->getLeft(), $request->getRight());
        } else {
            throw new \Exception(__("Error : Invalid operator"));
        }

        $this->responce->setResult($result);
        if ($request->getPrecision() != '') {
            $this->responce->setResult(number_format((float)$result, $request->getPrecision(), '.', ''));
        }
        $this->responce->setStatus("OK");
        return $this->responce;
    }

    /**
     * Calculate Addition
     *
     * @param float | int $leftValue
     * @param float | int $rightValue
     *
     * @return float | int
     */
    private function calculateAddition($leftValue, $rightValue)
    {
        return $leftValue + $rightValue;
    }

    /**
     * Calculate Subtraction
     *
     * @param float | int $leftValue
     * @param float | int $rightValue
     *
     * @return float | int
     */
    private function calculateSubtraction($leftValue, $rightValue)
    {
        return $leftValue - $rightValue;
    }

    /**
     * Calculate Multiplication
     *
     * @param float | int $leftValue
     * @param float | int $rightValue
     *
     * @return float | int
     */
    private function calculateMultiplication($leftValue, $rightValue)
    {
        return $leftValue * $rightValue;
    }

    /**
     * Calculate Divide
     *
     * @param float | int $leftValue
     * @param float | int $rightValue
     *
     * @return float | int
     */
    private function calculateDivide($leftValue, $rightValue)
    {
        return $leftValue / $rightValue;
    }

    /**
     * Calculate Power
     *
     * @param float | int $leftValue
     * @param float | int $rightValue
     *
     * @return float | int
     */
    private function calculatePower($leftValue, $rightValue)
    {
        return pow($leftValue, $rightValue);
    }
}
