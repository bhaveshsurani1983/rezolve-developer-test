<?php
/**
 * Bytes Technolab
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * php version 7.0
 *
 * @category Bytes
 * @package  Bytes_RecCalculater
 * @author   Magento Team <info@bytestechnolab.com>
 * @license  OSL 3.0
 * @link     http://www.bytestechnolab.com
 */
namespace Bytes\RecCalculator\Model;

use Magento\Framework\Model\AbstractModel;
use Bytes\RecCalculator\Api\Data\RequestInterface;

/**
 * Request model
 *
 */
class Request extends AbstractModel implements RequestInterface
{

    /**
     * Set Left value
     *
     * @param float | int $leftValue Left value
     *
     * @return DataInterface
     */
    public function setLeft($leftValue)
    {
        return $this->setData(RequestInterface::LEFT, $leftValue);
    }

    /**
     * Get Left value
     *
     * @return float | int
     */
    public function getLeft()
    {
        return $this->getData(RequestInterface::LEFT);
    }

    /**
     * Set Right value
     *
     * @param float | int $rightValue Right value
     *
     * @return DataInterface
     */
    public function setRight($rightValue)
    {
        return $this->setData(RequestInterface::RIGHT, $rightValue);
    }

    /**
     * Get Right value
     *
     * @return float | int
     */
    public function getRight()
    {
        return $this->getData(RequestInterface::RIGHT);
    }

    /**
     * Set Calculator Operator
     *
     * @param string $operator calulator operator
     *
     * @return DataInterface
     */
    public function setOperator($operator)
    {
        return $this->setData(RequestInterface::OPERATOR, $operator);
    }

    /**
     * Get Calculator Operator
     *
     * @return string
     */
    public function getOperator()
    {
        return $this->getData(RequestInterface::OPERATOR);
    }

    /**
     * Set Precision
     *
     * @param int $precision precision
     *
     * @return DataInterface
     */
    public function setPrecision($precision)
    {
        return $this->setData(RequestInterface::PRECISION, $precision);
    }

    /**
     * Get Precision
     *
     * @return int
     */
    public function getPrecision()
    {
        return $this->getData(RequestInterface::PRECISION);
    }
}
