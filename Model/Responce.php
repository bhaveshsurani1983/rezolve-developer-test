<?php
/**
 * Bytes Technolab
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * php version 7.0
 *
 * @category Bytes
 * @package  Bytes_RecCalculater
 * @author   Magento Team <info@bytestechnolab.com>
 * @license  OSL 3.0
 * @link     http://www.bytestechnolab.com
 */
namespace Bytes\RecCalculator\Model;

use Magento\Framework\Model\AbstractModel;
use Bytes\RecCalculator\Api\Data\ResponceInterface;

/**
 * Responce model
 *
 */
class Responce extends AbstractModel implements ResponceInterface
{

    /**
     * Set Status
     *
     * @param string $status Status
     *
     * @return DataInterface
     */
    public function setStatus($status)
    {
        return $this->setData(ResponceInterface::STATUS, $status);
    }

    /**
     * Get Status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->getData(ResponceInterface::STATUS);
    }

    /**
     * Set Result
     *
     * @param float | int $result Result
     *
     * @return DataInterface
     */
    public function setResult($result)
    {
        return $this->setData(ResponceInterface::RESULT, $result);
    }

    /**
     * Get Result
     *
     * @return float | int
     */
    public function getResult()
    {
        return $this->getData(ResponceInterface::RESULT);
    }
}
