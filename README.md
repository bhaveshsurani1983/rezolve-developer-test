RecCalculator Extension for Magneto 2 developed by Bytes Technolab

Welcome to RecCalculator Extension for Magneto 2 developed by Bytes Technolab.

The extension provide api for calculate "Addition", "Subtration", "Multiplication", "Divide" and "Power" operationns.

##Endpoint
POST: /V1/api/rce/calculator

POST params
{
	"request":{
		"left": int|float,
		"right": int|float,
		"operator": string,
		"precision": int,
	}
}

##Support: 
version - 2.1.x, 2.2.x, 2.3.x

##How to install Extension

1. Download the archive file.
2. Unzip the files
3. Create a folder [Magento_Root]/app/code/Bytes/RecCalculator
4. Drop/move the unzipped files

#Enable Extension:

	php bin/magento module:enable Bytes_RecCalculator
	php bin/magento setup:upgrade
	php bin/magento cache:clean
	php bin/magento setup:static-content:deploy
	php bin/magento cache:flush

#Disable Extension:

	php bin/magento module:disable Bytes_RecCalculator
	php bin/magento setup:upgrade
	php bin/magento cache:clean
	php bin/magento setup:static-content:deploy
	php bin/magento cache:flush